// yarn install => package de dependências
// yarn add express
// Require => express
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

// Instância do app ("main")
const app = express();

// Real Time reload with Socket.io
const server = require('http').Server(app);
const io = require('socket.io')(server);

// Conecção com Banco de Dados FREE => https://mlab.com/databases/omnistack-week#users
mongoose.connect("mongodb://vitornovaes:vn123456@ds123635.mlab.com:23635/omnistack-week", {
    userNewUrlParser: true
});

// Middle Real Time : 
app.use( (req, res, next) => {
    req.io = io;
    return next();
});

app.use(cors());
// Require routes => express.router
app.use(express.json());
app.use(require('./routes'));

server.listen(3000, () => {
    console.log('http://localhost:3000/ Server started =========================');
});

// Install : yarn add Mongoose (ORM) e nodemon -D (Real Time restart server)
