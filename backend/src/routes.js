const express = require('express');
const routes = express.Router();

const TweetController = require("../controllers/TweetController");
const LikeController = require("../controllers/LikeController");

//req => requisição do servidor (Authenticate, Query Params, Params ...)
//res => requisição do servidor (Resposta Front)
// routes.get('/',(req,res) => {
//     return res.send("Hello World!");
// });

routes.get('/tweets', TweetController.index);
routes.post('/tweets', TweetController.store);
routes.post('/likes/:id', LikeController.store);

module.exports = routes;