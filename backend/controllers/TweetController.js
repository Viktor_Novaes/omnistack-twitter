const Tweet = require('../src/models/Tweet');

module.exports = {

    // REQ "/" GET (Listagem de Tweets)
    async index(req,res){
      const tweets = await Tweet.find({}).sort("-createAt");
      return res.json(tweets);
    },

    // REQ "/" POST (Novo Tweet)
    async store(req, res) {
        const tweet = await Tweet.create(req.body);    

        // Real Time
        req.io.emit('tweet', tweet);

        return res.json(tweet);

    }

};