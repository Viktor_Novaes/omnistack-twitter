const Tweet = require('../src/models/Tweet');

module.exports = {
    async store(req,res){
        const tweet = await Tweet.findById(req.params.id);
        tweet.set({ likes: tweet.likes + 1});

        // Real Time
        req.io.emit('like', tweet);

        tweet.save();
        return res.json(tweet);

    }
};