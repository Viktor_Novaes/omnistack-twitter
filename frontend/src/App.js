import React, { Component } from 'react';
// yarn add react-router-dom
import { BrowserRouter, StaticRouter, Route } from 'react-router-dom';
import Switch from 'react-router-dom/Switch';

// Imports dos Components para cada página
import Login from './pages/Login';
import TimeLine from './pages/Timeline';


class App extends Component {
  render() {
    return ( 
      // Rotas do componente
      <BrowserRouter>
        < Switch > 
          <Route path="/" exact component={Login} />
          <Route path="/feed" component={TimeLine} />
        </ Switch >
      </BrowserRouter>
    );
  }
}

export default App;
